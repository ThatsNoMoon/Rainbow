package com.thatsnomoon.rainbow

import com.mongodb.MongoClient
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters.eq
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.Role
import org.bson.Document

object DB {

    private val mongoClient: MongoClient = MongoClient()
    private val rainbowGuilds: MongoCollection<Document>

    private val DEFAULT_PREFIX = "r!"

    init {
        rainbowGuilds = mongoClient.getDatabase("rainbow").getCollection("guilds")
    }

//    fun addFirstGuild() {
//        val usersDoc = Document("257711607096803328", 360186280912486410)
//                .append("261777370568982528", 360186273631043604)
//        val doc = Document("_id", 261372109383663616)
//                .append("prefix", "r!")
//                .append("enabled_roles", listOf(345018568355872780))
//                .append("user_roles", usersDoc)
//        rainbowGuilds.insertOne(doc)
//    }

    fun getPrefix(id: Long): String {
        val guild = rainbowGuilds.find(eq("_id", id)).first() ?: return DEFAULT_PREFIX
        val prefix = guild["prefix"]?: DEFAULT_PREFIX
        return prefix.toString()
    }

    fun getPrefix(guild: Guild): String {
        return getPrefix(guild.idLong)
    }

    fun setPrefix(guildId: Long, value: String) {
        rainbowGuilds.updateOne(Document("_id", guildId), Document("\$set", Document("prefix", value)))
    }

    fun setPrefix(guild: Guild, value: String) {
        setPrefix(guild.idLong, value)
    }

    private fun setRoleEnabled(guildId: Long, roleId: Long, value: Boolean) {
        var guild = rainbowGuilds.find(eq("_id", guildId)).first()
        if (guild == null) {
            rainbowGuilds.insertOne(Document("_id", guildId))
            guild = rainbowGuilds.find(eq("_id", guildId)).first()
        }

        val roles = guild["enabled_roles"]?: listOf<Long>()
        if (roles is List<*>) {
            if (roles.contains(roleId)) {
                if (value) return
                else {
                    rainbowGuilds.updateOne(Document("_id", guildId), Document("\$pull", Document("enabled_roles", roleId)))
                }
            }
        }
        if (value)
            rainbowGuilds.updateOne(Document("_id", guildId), Document("\$push", Document("enabled_roles", roleId)))
    }

    fun enableRole(guildId: Long, roleId: Long) {
        setRoleEnabled(guildId, roleId, true)
    }

    fun disableRole(guildId: Long, roleId: Long) {
        setRoleEnabled(guildId, roleId, false)
    }

    fun enableRole(guild: Guild, role: Role) {
        enableRole(guild.idLong, role.idLong)
    }

    fun disableRole(guild: Guild, role: Role) {
        disableRole(guild.idLong, role.idLong)
    }

    fun hasEnabledRole(member: Member): Boolean {
        val guild = rainbowGuilds.find(eq("_id", member.guild.idLong)).first()?: return false
        val enabledRoles = guild["enabled_roles"]?: return false
        val memberRoles = member.roles
        if (enabledRoles == "all")
            return true
        return if (enabledRoles is List<*>)
            memberRoles.stream().anyMatch { enabledRoles.contains(it.idLong) }
        else
            false
    }

    fun getMemberColorRole(guildId: Long, memberId: Long): Long? {
        val guild = rainbowGuilds.find(eq("_id", guildId)).first()?: return null
        val userRoles = guild["user_roles"]?: return null
        return if (userRoles is Document) {
            val result = userRoles[memberId.toString()]
            result as? Long
        }
        else
            null

    }

    fun getMemberColorRole(guild: Guild, member: Member): Role? {
        val roleId = getMemberColorRole(guild.idLong, member.user.idLong)?: return null
        return guild.getRoleById(roleId)
    }

    fun setMemberColorRole(guildId: Long, roleId: Long, memberId: Long) {
        rainbowGuilds.updateOne(eq("_id", guildId), Document("\$set", Document("user_roles.$memberId", roleId)))
    }

    fun setMemberColorRole(role: Role, member: Member) {
        setMemberColorRole(member.guild.idLong, role.idLong, member.user.idLong)
    }
}