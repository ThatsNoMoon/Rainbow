package com.thatsnomoon.rainbow

import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.LoggerContext
import org.apache.logging.log4j.core.appender.ConsoleAppender
import org.apache.logging.log4j.core.config.Configuration
import org.apache.logging.log4j.core.config.ConfigurationFactory
import org.apache.logging.log4j.core.config.ConfigurationSource
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilder
import org.apache.logging.log4j.core.config.builder.impl.BuiltConfiguration
import java.net.URI

class Log4J2Config: ConfigurationFactory() {

    private fun createConfiguration(name: String, builder: ConfigurationBuilder<BuiltConfiguration>): Configuration {
        builder.setConfigurationName(name)
        /* internal log4j2 messages will only be displayed if they are WARN or higher */
        builder.setStatusLevel(Level.WARN)
        /* new appender to output to console */
        val appenderBuilder = builder.newAppender("STDOUT", "CONSOLE")
                .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
        /* pattern to apply to output to console */
        appenderBuilder.add(builder.newLayout("PatternLayout")
                /* boring pattern */
//                .addAttribute("pattern", "[%d{HH:mm:ss}] (%t) [%c{1}] %level: %m%n%throwable"));
                /* colorful pattern! */
                .addAttribute("pattern", "%style{[%d{HH:mm:ss}]}{white} %style{[%c{1}]}{blue} %highlight{%level: %m%n%throwable}"))
        builder.add(appenderBuilder)
        /* set up root logger to log to the console appender */
        builder.add(builder.newRootLogger(Level.INFO).add(builder.newAppenderRef("STDOUT")))
        return builder.build()
    }

    override fun getConfiguration(loggerContext: LoggerContext?, source: ConfigurationSource?): Configuration {
        return getConfiguration(loggerContext, source.toString(), null)
    }

    override fun getConfiguration(loggerContext: LoggerContext?, name: String, configLocation: URI?): Configuration {
        val builder = newConfigurationBuilder()
        return createConfiguration(name, builder)
    }

    override fun getSupportedTypes(): Array<String> {
        return arrayOf("*")
    }
}