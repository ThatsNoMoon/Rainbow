package com.thatsnomoon.rainbow

import org.apache.logging.log4j.core.config.ConfigurationFactory

fun main(args: Array<String>) {
    ConfigurationFactory.setConfigurationFactory(Log4J2Config())
    Rainbow()
}