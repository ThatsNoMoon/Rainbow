package com.thatsnomoon.rainbow

import club.minnced.kjda.SPACES
import club.minnced.kjda.entities.sendTextAsync
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

class MessageListener(private val parent: Rainbow): ListenerAdapter() {

    override fun onGuildMessageReceived(event: GuildMessageReceivedEvent) {
        if (event.author.isBot)
            return
        if (event.message.contentRaw.startsWith("<@${event.jda.selfUser.idLong}>")) {
            if (event.message.contentRaw.split(SPACES).size > 1) {
                if (event.message.contentRaw.split(SPACES)[1] == "help") {
                    event.channel.sendTextAsync { "Prefix: ${DB.getPrefix(event.guild)}" }
                }
            }
        }
        val prefix = DB.getPrefix(event.guild)
        if (!event.message.contentDisplay.toLowerCase().startsWith(prefix.toLowerCase()))
            return
        val parts = event.message.contentDisplay.toLowerCase().substring(prefix.length).split(SPACES)
        if (parts.isEmpty())
            return
        val commandText = parts[0]
        val args = if (parts.size > 1)
            parts.subList(1, parts.size)
        else
            listOf()
        val command = parent.commands[commandText]?: return
        command.execute(event.message, args)
    }
}