package com.thatsnomoon.rainbow

import com.thatsnomoon.kda.buildClient
import com.thatsnomoon.rainbow.commands.*
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.entities.Game

class Rainbow {

    val commands: Map<String, Command> = run {
        val tempMap = mutableMapOf<String, Command>()
        for (c in orderedCommands) {
            for (a in c.aliases) {
                tempMap.put(a, c)
            }
        }
        tempMap
    }

    val orderedCommands: Array<Command> = arrayOf(EnableRole(), DisableRole(), Help(), Prefix(), Preview(), SetColor(), SetUserRole())

    val jda: JDA = buildClient(AccountType.BOT) {
        setToken(System.getenv("DISCORD_TOKEN"))
        addEventListener(MessageListener(this@Rainbow))
        setGame(Game.playing("r!help"))
    }
}