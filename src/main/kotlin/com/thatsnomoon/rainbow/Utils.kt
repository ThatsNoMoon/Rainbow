package com.thatsnomoon.rainbow

import java.awt.Color

val FRIENDLY_COLORS = mapOf<String, Color>(
        Pair("black", Color.BLACK),
        Pair("blue", Color.BLUE),
        Pair("cyan", Color.CYAN),
        Pair("dark_gray", Color.DARK_GRAY),
        Pair("gray", Color.GRAY),
        Pair("light_gray", Color.LIGHT_GRAY),
        Pair("magenta", Color.MAGENTA),
        Pair("orange", Color.ORANGE),
        Pair("pink", Color.PINK),
        Pair("red", Color.RED),
        Pair("yellow", Color.YELLOW)
)

fun String.toRGB(): Triple<Int, Int, Int>? {
    var op = this.toLowerCase()
    if (op.startsWith('#'))
        op = op.substring(1)
    var r: Int? = null
    var g: Int? = null
    var b: Int? = null

    val color = FRIENDLY_COLORS[op]
    if (color != null) {
        return Triple(color.red, color.green, color.blue)
    }

    when (op.length) {
        6 -> {
            r = op.substring(0, 2).toIntOrNull(16)
            g = op.substring(2, 4).toIntOrNull(16)
            b = op.substring(4).toIntOrNull(16)
        }
        3 -> {
            r = op.substring(0, 1).toIntOrNull(16)
            g = op.substring(1, 2).toIntOrNull(16)
            b = op.substring(2).toIntOrNull(16)
            if (r != null && g != null && b != null) {
                r += r * 16
                g += g * 16
                b += b * 16
            }
        }
    }
    if (r == null || g == null || b == null) {
        return null
    }
    return Triple(r, g, b)
}