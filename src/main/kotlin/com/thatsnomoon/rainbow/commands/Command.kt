package com.thatsnomoon.rainbow.commands

import net.dv8tion.jda.core.entities.Message

abstract class Command {
    abstract fun execute(msg: Message, args: List<String>)

    abstract val aliases: List<String>

    abstract val usage: String
}