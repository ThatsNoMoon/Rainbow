package com.thatsnomoon.rainbow.commands

import com.jagrosh.jdautilities.utils.FinderUtil
import com.thatsnomoon.kda.extensions.replyAsync
import com.thatsnomoon.rainbow.DB
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Message

class DisableRole : Command() {

    override fun execute(msg: Message, args: List<String>) {
        if (!msg.member.hasPermission(Permission.ADMINISTRATOR)) {
            msg.replyAsync { "You can't do that!" }
            return
        }
        if (args.isEmpty()) {
            msg.replyAsync { usage }
            return
        }
        val roles = FinderUtil.findRoles(args.joinToString(" "), msg.guild)
        if (roles.isEmpty()) {
            msg.replyAsync { "Couldn't find role ${args.joinToString(" ")}!" }
            return
        }
        val role = roles[0]
        DB.disableRole(msg.guild, role)
        msg.replyAsync { "Disabled `setcolor` for ${role.name}" }
    }

    override val aliases: List<String> = listOf("disablerole", "disable", "dr")
    override val usage: String = "Usage: `disablerole [role]`\n`role` can be the name of a role, a role mention, or a role ID."

}