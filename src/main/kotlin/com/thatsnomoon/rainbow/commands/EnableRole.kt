package com.thatsnomoon.rainbow.commands

import club.minnced.kjda.entities.sendTextAsync
import com.jagrosh.jdautilities.utils.FinderUtil
import com.thatsnomoon.rainbow.DB
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Message

class EnableRole : Command() {

    override fun execute(msg: Message, args: List<String>) {
        if (!msg.member.hasPermission(Permission.ADMINISTRATOR)) {
            msg.channel.sendTextAsync { "You can't do that!" }
            return
        }
        if (args.isEmpty()) {
            msg.channel.sendTextAsync { usage }
            return
        }
        val roles = FinderUtil.findRoles(args.joinToString(" "), msg.guild)
        if (roles.isEmpty()) {
            msg.channel.sendTextAsync { "Couldn't find role ${args.joinToString(" ")}!" }
            return
        }
        val role = roles[0]
        DB.enableRole(msg.guild, role)
        msg.channel.sendTextAsync { "Enabled `setcolor` for ${role.name}" }
    }

    override val aliases: List<String> = listOf("enablerole", "enable", "er")
    override val usage: String = "Usage: `enablerole [role]`\n`role` can be the name of a role, a role mention, or a role ID"

}