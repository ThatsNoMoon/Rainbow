package com.thatsnomoon.rainbow.commands

import com.thatsnomoon.kda.extensions.replyAsync
import net.dv8tion.jda.core.entities.Message

class Help: Command() {
    override fun execute(msg: Message, args: List<String>) {
        if (args.isEmpty()) {
            msg replyAsync {
                """
                |Taste the Rainbow
                """
            }
        }
    }

    override val aliases: List<String> = listOf("help", "h")
    override val usage: String = "Usage: `help (command)`"
}