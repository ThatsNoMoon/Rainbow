package com.thatsnomoon.rainbow.commands

import club.minnced.kjda.entities.sendTextAsync
import com.thatsnomoon.rainbow.DB
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Message

class Prefix: Command() {

    override fun execute(msg: Message, args: List<String>) {
        if (!msg.member.hasPermission(Permission.ADMINISTRATOR)) {
            msg.channel.sendTextAsync { "You can't do that!" }
            return
        }
        if (args.isEmpty()) {
            msg.channel.sendTextAsync { usage }
            return
        }
        DB.setPrefix(msg.guild, args.joinToString(" "))
        msg.channel.sendTextAsync { "Set ${msg.guild.name}'s prefix to ${args.joinToString(" ")}" }
    }

    override val aliases: List<String> = listOf("prefix")

    override val usage: String = "Usage: `prefix [new prefix]`"

}