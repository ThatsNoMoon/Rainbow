package com.thatsnomoon.rainbow.commands

import club.minnced.kjda.entities.sendTextAsync
import club.minnced.kjda.message
import com.thatsnomoon.rainbow.toRGB
import net.dv8tion.jda.core.entities.Message
import java.awt.Color
import java.awt.image.BufferedImage
import java.awt.image.BufferedImage.TYPE_INT_RGB
import java.io.File
import javax.imageio.ImageIO

class Preview: Command() {

    override fun execute(msg: Message, args: List<String>) {
        val colorStr = args[0]
        val result = colorStr.toRGB()
        if (result == null) {
            msg.channel.sendTextAsync { usage }
            return
        }
        val (r, g, b) = result
        val img = BufferedImage(100, 100, TYPE_INT_RGB)
        val graphics = img.createGraphics()
        graphics.background = Color(r, g, b)
        graphics.clearRect(0,0,100,100)
        val pic = File.createTempFile(colorStr, ".png")
        ImageIO.write(img, "jpg", pic)
        msg.channel.sendFile(pic, message { append("Preview:") }).queue()
    }

    override val aliases: List<String> = listOf("preview")

    override val usage: String = "Usage: `preview [color]`\nColor must be either a color name or an HTML color code.\nSee here for more info about HTML Color Codes: https://html-color-codes.info/"
}