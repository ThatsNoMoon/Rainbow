package com.thatsnomoon.rainbow.commands

import club.minnced.kjda.entities.sendTextAsync
import com.thatsnomoon.rainbow.DB
import com.thatsnomoon.rainbow.toRGB
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Message
import java.awt.Color

class SetColor: Command() {

    override fun execute(msg: Message, args: List<String>) {
        if (!msg.guild.selfMember.hasPermission(Permission.MANAGE_ROLES)) {
            msg.channel.sendTextAsync { "Sorry, I need the `Manage Roles` permission to do that!" }
            return
        }
        if (!DB.hasEnabledRole(msg.member)) {
            msg.channel.sendTextAsync { "Your role doesn't have setcolor enabled!" }
            return
        }
        if (args.isEmpty()) {
            msg.channel.sendTextAsync { usage }
            return
        }
        val colorTriple = args[0].toRGB()
        if (colorTriple == null) {
            msg.channel.sendTextAsync { usage }
            return
        }
        val role = DB.getMemberColorRole(msg.guild, msg.member)
        val (r, g, b) = colorTriple
        val color = Color(r, g, b)
        if (role == null) {
            msg.guild.controller.createRole().setName("${msg.author.name}#${msg.author.discriminator}").setColor(Color(r, g, b)).queue {
                if (!msg.member.roles.contains(it)) {
                    msg.guild.controller.addSingleRoleToMember(msg.member, it).queue()
                    DB.setMemberColorRole(it, msg.member)
                }
                msg.channel.sendTextAsync { "Done! Set your color to ${args[0]}" }
                msg.guild.controller.modifyRolePositions(false).selectPosition(it).moveTo(msg.guild.roles.indexOf(msg.guild.selfMember.roles[0]) + 1).queue()
            }
            return
        }
        role.manager.setColor(color).queue {
            if (!msg.member.roles.contains(role))
                msg.guild.controller.addSingleRoleToMember(msg.member, role).queue()
            msg.channel.sendTextAsync { "Done! Set your color to ${args[0]}" }
        }
        msg.guild.controller.modifyRolePositions(false).selectPosition(role).moveTo(msg.guild.roles.indexOf(msg.guild.selfMember.roles[0]) + 1).queue()
    }


    override val aliases: List<String> = listOf("setcolor", "setc", "set", "sc")
    override val usage: String = "Usage: `setcolor [color]`\n`color` must be either a color name or an HTML color code.\nSee here for more info about HTML Color Codes: https://html-color-codes.info/"

}