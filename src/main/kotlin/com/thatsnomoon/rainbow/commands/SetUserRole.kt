package com.thatsnomoon.rainbow.commands

import club.minnced.kjda.entities.sendTextAsync
import com.jagrosh.jdautilities.utils.FinderUtil
import com.thatsnomoon.rainbow.DB
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Message

class SetUserRole: Command() {

    override fun execute(msg: Message, args: List<String>) {
        if (!msg.member.hasPermission(Permission.ADMINISTRATOR)) {
            msg.channel.sendTextAsync { "You can't do that!" }
            return
        }
        if (args.size < 2) {
            msg.channel.sendTextAsync { usage }
            return
        }
        val members = FinderUtil.findMembers(args[0], msg.guild)
        if (members.isEmpty()) {
            msg.channel.sendTextAsync { "Couldn't find user ${args[0]}" }
            return
        }
        val member = members[0]
        val roles = FinderUtil.findRoles(args[1], msg.guild)
        if (roles.isEmpty()) {
            msg.channel.sendTextAsync { "Couldn't find role ${args[1]}" }
            return
        }
        val role = roles[0]
        DB.setMemberColorRole(role, member)
        msg.channel.sendTextAsync { "Set ${member.effectiveName}'s color role to ${role.name}." }
    }

    override val aliases: List<String> = listOf("setuserrole", "setur", "sur")
    override val usage: String = "Usage: `setuserrole [user] [role]`\n`user` can be a username, nickname, mention, or user ID.\n`role` can be a role name, mention, or role ID."
}